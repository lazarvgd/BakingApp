package com.example.svarog.bakingapp.db;

import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by svarog on 9.12.17..
 */

public final class RecipeContract {

    public static final String AUTHORITY = "com.example.svarog.bakingapp";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + AUTHORITY);
    public static final String PATH_RECIPE = "favoriteRecipe";

    private RecipeContract() {
    }

    public static class RecipeEntry implements BaseColumns {

        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_RECIPE).build();

        public static final String TABLE = "favoriteRecipe";

        public static final String QUANTITY = "quantity";

        public static final String MEASURE = "measure";

        public static final String INGREDIENT = "ingredient";


    }

}
