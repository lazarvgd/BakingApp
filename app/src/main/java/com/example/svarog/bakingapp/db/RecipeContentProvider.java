package com.example.svarog.bakingapp.db;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import static com.example.svarog.bakingapp.db.RecipeContract.AUTHORITY;
import static com.example.svarog.bakingapp.db.RecipeContract.PATH_RECIPE;

/**
 * Created by svarog on 9.12.17..
 */

public class RecipeContentProvider extends ContentProvider {


    public static final int RECIPES = 100;
    public static final int RECIPE_WITH_ID = 101;


    private static final UriMatcher sUriMatcher = buildUriMatcher();
    private RecipeDbHelper recipeDbHelper;


    public static UriMatcher buildUriMatcher() {
        UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

        uriMatcher.addURI(AUTHORITY, PATH_RECIPE, RECIPES);

        uriMatcher.addURI(AUTHORITY, PATH_RECIPE + "/#", RECIPE_WITH_ID);

        return uriMatcher;
    }

    @Override
    public boolean onCreate() {

        Context context = getContext();

        recipeDbHelper = new RecipeDbHelper(context);


        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri,
                        @Nullable String[] projection,
                        @Nullable String selection,
                        @Nullable String[] selectionArgs,
                        @Nullable String order) {

        final SQLiteDatabase db = recipeDbHelper.getReadableDatabase();

        int match = sUriMatcher.match(uri);

        Cursor cursor;


        switch (match) {
            case RECIPES:

                cursor = db.query(
                        RecipeContract.RecipeEntry.TABLE,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        order);
                break;


                default:
                    throw new UnsupportedOperationException("Unknown uri " + uri);
        }

        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {


        final SQLiteDatabase db = recipeDbHelper.getWritableDatabase();

        int match = sUriMatcher.match(uri);

        Uri returnUri;

        switch (match) {

            case RECIPES:
                long id = db.insert(RecipeContract.RecipeEntry.TABLE, null, contentValues);
                if (id > 0) {
                    returnUri = ContentUris.withAppendedId(RecipeContract.RecipeEntry.CONTENT_URI, id);
                } else {
                    throw new SQLException("Failed to insert row into " + uri);
                }


                break;

            default:
                throw new UnsupportedOperationException("Unkown uri " + uri);

        }

        getContext().getContentResolver().notifyChange(uri, null);

//        throw new UnsupportedOperationException("not yet implemented");
        return returnUri;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String s, @Nullable String[] strings) {
        final SQLiteDatabase db = recipeDbHelper.getWritableDatabase();
        switch (sUriMatcher.match(uri)) {
            case RECIPES:
                db.delete(RecipeContract.RecipeEntry.TABLE, s, strings);
        break;
        }
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues contentValues, @Nullable String s, @Nullable String[] strings) {
        return 0;
    }
}
