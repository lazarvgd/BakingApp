package com.example.svarog.bakingapp.helpers;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.svarog.bakingapp.R;
import com.example.svarog.bakingapp.models.Recipe;
import com.example.svarog.bakingapp.models.Step;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

/**
 * Created by svarog on 11.11.17..
 */

public class RecipeListAdapter extends RecyclerView.Adapter<RecipeListAdapter.RecipeViewHolder> {
    private ListItemClickListener mListItemClickListener;
    private  Context context;
    private List<Recipe> mRecipes;



    public RecipeListAdapter(List<Recipe> recipes, ListItemClickListener listItemClickListener, Context context) {
        this.mRecipes = recipes;
        this.mListItemClickListener = listItemClickListener;
        this.context =context;
    }

    @Override
    public RecipeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recipe_item_viewholder, parent, false);

        return new RecipeViewHolder(item);
    }

    @Override
    public void onBindViewHolder(RecipeViewHolder holder, int position) {

        holder.recipeName.setText(mRecipes.get(position).getName());
        holder.bindRecipeImg(mRecipes.get(position));
    }


    @Override
    public int getItemCount() {
        return mRecipes.size();
    }


    public class RecipeViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.recipe_name)
        TextView recipeName;
        @BindView(R.id.recipe_img)
        ImageView recipeImg;

        public RecipeViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);

        }


        public void bindRecipeImg(Recipe recipe) {
            String recipeImage = "";
        Timber.d("Recipe image " + recipe.getImage() + " is empty : " +recipe.getImage().isEmpty());
            recipeImage = recipe.getImage();

            if(recipeImage.equals("") || recipeImage == null){
                Timber.d("EMPTY");
                Picasso.with(context).load(R.drawable.icon_cookies).centerInside().resize(50,50).into(recipeImg);

            }else {
                Timber.d("URL : " + recipeImage) ;

                Picasso.with(context).load(recipeImage).centerInside().resize(50,50).into(recipeImg);
            }

        }


        @Override
        public void onClick(View view) {
            int pos = getAdapterPosition();
            mListItemClickListener.onListItemClick(pos);
        }


    }


}
