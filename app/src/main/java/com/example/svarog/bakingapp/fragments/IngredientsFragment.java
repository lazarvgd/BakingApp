package com.example.svarog.bakingapp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.svarog.bakingapp.R;
import com.example.svarog.bakingapp.helpers.IngredientListAdapter;
import com.example.svarog.bakingapp.helpers.ListItemClickListener;
import com.example.svarog.bakingapp.models.Ingredient;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

/**
 * Created by svarog on 20.11.17..
 */

public class IngredientsFragment extends Fragment {

    @BindView(R.id.ingredients_list)
    RecyclerView ingredientsRecyclerViewList;
    private IngredientListAdapter adapter;
    List<Ingredient> ingredientList;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.ingredients_fragment_layout,container,false);
        ButterKnife.bind(this,root);
        if(getArguments().containsKey("ingredientsRecyclerViewList")){
            Ingredient[] ingredients = (Ingredient[]) getArguments().getParcelableArray("ingredientsRecyclerViewList");
            ingredientList = new ArrayList<>(Arrays.asList(ingredients));
            ingredients= null;

            Timber.d("FROM Ingredients fragment!");
            for (Ingredient ingredient : ingredientList){
                Timber.d("ingredinet " + ingredient.getIngredient() );
            }

            adapter = new IngredientListAdapter(ingredientList);
            ingredientsRecyclerViewList.setLayoutManager(new LinearLayoutManager(getContext()));
            ingredientsRecyclerViewList.setAdapter(adapter);
        }


        return root;

    }


}
