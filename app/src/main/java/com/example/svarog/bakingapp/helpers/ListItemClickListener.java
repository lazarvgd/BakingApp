package com.example.svarog.bakingapp.helpers;

/**
 * Created by svarog on 12.11.17..
 */

public interface ListItemClickListener {


    public void onListItemClick(int clickItmeIndex);
}
