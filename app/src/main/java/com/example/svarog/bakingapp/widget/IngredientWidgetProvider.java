package com.example.svarog.bakingapp.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import com.example.svarog.bakingapp.R;
import com.example.svarog.bakingapp.activities.MainActivity;
import com.example.svarog.bakingapp.async.GetIngredientsForWidget;

import java.util.concurrent.ExecutionException;

import timber.log.Timber;

/**
 * Implementation of App Widget functionality.
 */
public class IngredientWidgetProvider extends AppWidgetProvider {

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {
        String ingredients = context.getString(R.string.no_recipe);

        //TODO: ASYNC GET DATA
        GetIngredientsForWidget getIngredientsForWidget = new GetIngredientsForWidget(context);
        try {
            ingredients = getIngredientsForWidget.execute().get().toString();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        if (ingredients.equals(""))
            ingredients = context.getString(R.string.no_recipe);
        // Construct the RemoteViews object
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.ingredient_widget_provider);
        views.setTextViewText(R.id.ingredients_widget_layout, ingredients);


        Intent intent = new Intent(context, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
// Get the layout for the App Widget and attach an on-click listener to the button

        //issue - couldn't create widget on the screen
        views.setOnClickPendingIntent(R.id.ingredients_widget_layout, pendingIntent);

        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        if(appWidgetIds ==null){
            return;
        }
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);

        }

//        Intent intent = new Intent(context, MainActivity.class);
//        context.startActivity(intent);

    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        AppWidgetManager mgr = AppWidgetManager.getInstance(context);
        Timber.d("ID :  " + intent.getIntArrayExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS));
        onUpdate(context,mgr, intent.getIntArrayExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS));
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }
}

