package com.example.svarog.bakingapp.fragments;

import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.svarog.bakingapp.R;
import com.example.svarog.bakingapp.async.SaveIngredientsAsync;
import com.example.svarog.bakingapp.helpers.DeviceOrientationUtils;
import com.example.svarog.bakingapp.helpers.ListItemClickListener;
import com.example.svarog.bakingapp.helpers.RecipeIngredientsStepsAdapter;
import com.example.svarog.bakingapp.models.Ingredient;
import com.example.svarog.bakingapp.models.Recipe;
import com.example.svarog.bakingapp.models.Step;
import com.example.svarog.bakingapp.widget.IngredientWidgetProvider;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

/**
 * Created by svarog on 19.11.17..
 */

public class RecipeFragment extends Fragment implements ListItemClickListener {


    @BindView(R.id.ingredientStepList)
    RecyclerView ingredientStepList;
    private Recipe recipe;
    private RecipeIngredientsStepsAdapter adapter;
    private boolean isFirstStart = true;
    private int lastClickedIndex;
    private List<Ingredient> ingredients;
    private int fragmentContainer = R.id.ingridients_step_list_fragment_container;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.recipe_fragment_layout, container, false);

        ButterKnife.bind(this, root);

        setHasOptionsMenu(true);

        isFirstStart = false;

        Bundle data = getArguments();

        if (data.containsKey("recipeData")) {
//            Timber.d("recipe in fragment found!");
            recipe = data.getParcelable("recipeData");

            ingredients = recipe.getIngredients();
            List<Step> steps = recipe.getSteps();

            ingredientStepList.setLayoutManager(new LinearLayoutManager(getContext()));
            adapter = new RecipeIngredientsStepsAdapter(this);
            adapter.setIngredients(ingredients);
            adapter.setSteps(steps);
            ingredientStepList.setAdapter(adapter);

//            if (savedInstanceState != null && savedInstanceState.containsKey("key")) {
//                onListItemClick(savedInstanceState.getInt("key"));
////                Timber.d("It contains a key, here is it : " + savedInstanceState.getInt("key"));
//            } else {
////                Timber.d("NO KEY!");
//            }


        } else {
            Timber.d("recipe missing in fragment");

            //To be honest, this works fine. I have 7 exams in a 15 days, so I really think that it is not necessary to use snackbar in order to fulfill material design standards.
            //Also getting CoordinatorLayout and passing to snackbar is not that easy as Toast. Please bare with me.

            Toast.makeText(getContext(), "No recipe data at the moment!", Toast.LENGTH_SHORT).show();
            onStop();
        }

        return root;
    }

    @Override
    public void onListItemClick(int clickItmeIndex) {
        lastClickedIndex = clickItmeIndex;

        if (DeviceOrientationUtils.isDeciceATablet()) {
            fragmentContainer = R.id.ingredients_steps_container;
        }

        if (clickItmeIndex == 0) {
            IngredientsFragment ingredientsFragment = new IngredientsFragment();
            Bundle data = new Bundle();
            Ingredient[] parc = recipe.getIngredients().toArray(new Ingredient[recipe.getIngredients().size()]);

            data.putParcelableArray("ingredientsRecyclerViewList", parc);

            ingredientsFragment.setArguments(data);

            getFragmentManager()
                    .beginTransaction()
                    .addToBackStack(null)
                    .replace(fragmentContainer, ingredientsFragment)
                    .commit();
            Timber.d("Recipe is : " + recipe.getName());
            for (Ingredient ingredient : recipe.getIngredients()) {
                Timber.d("Ingredient + " + ingredient.getIngredient());
            }

        } else {
            Timber.d("Recipe is : " + recipe.getName());
            StepFragment stepFragment = new StepFragment();
            Bundle data = new Bundle();
            ArrayList<Step> steps = (ArrayList<Step>) recipe.getSteps();

            data.putParcelableArrayList("steps", steps);
            //NOT NEEDED TO REDUCE THE INDEX NUMBER, due to IF statement on line 103

            data.putInt("page", clickItmeIndex-1);
            data.putBoolean("orientation", true);
            stepFragment.setArguments(data);

            getFragmentManager()
                    .beginTransaction()
                    .addToBackStack(null)
                    .replace(fragmentContainer, stepFragment)
                    .commit();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("key", lastClickedIndex);

    }

    @Override
    public void onResume() {
        super.onResume();

    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_layout, menu);

        return;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.save_recipe:
                final boolean hasDataInDb = false;
                Timber.d("Recipe saved");

                SaveIngredientsAsync save = new SaveIngredientsAsync(getContext(), recipe.getIngredients());
                save.execute();

                updateWidget();

                return true;
        }
        return false;
    }

    private void updateWidget() {
        Intent intent = new Intent(getContext(), IngredientWidgetProvider.class);
        intent.setAction("android.appwidget.action.APPWIDGET_UPDATE");
        int ids[] = AppWidgetManager.getInstance(getActivity()).getAppWidgetIds(new ComponentName(getActivity(), IngredientWidgetProvider.class));
        Timber.d("IDS : " + ids.length);
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS,ids);
        getContext().sendBroadcast(intent);

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();


    }
}
