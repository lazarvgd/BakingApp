package com.example.svarog.bakingapp.application;

import android.app.Application;
import android.content.Context;
import android.util.DisplayMetrics;

import com.example.svarog.bakingapp.R;
import com.facebook.stetho.Stetho;

import timber.log.Timber;

/**
 * Created by svarog on 11.11.17..
 */

public class BakingApplication extends Application {

    public static final String BASE_URL = "https://d17h27t6h515a5.cloudfront.net/";
    private static BakingApplication bakingApplication;

    public void onCreate() {
        super.onCreate();
        bakingApplication = this;
        Timber.plant(new Timber.DebugTree());

        Stetho.initializeWithDefaults(this);
    }

    public static BakingApplication getInstance() {
        return bakingApplication;
    }


}
