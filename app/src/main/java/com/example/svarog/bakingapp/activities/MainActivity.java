package com.example.svarog.bakingapp.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.svarog.bakingapp.R;
import com.example.svarog.bakingapp.helpers.ListItemClickListener;
import com.example.svarog.bakingapp.helpers.RecipeListAdapter;
import com.example.svarog.bakingapp.models.Recipe;
import com.example.svarog.bakingapp.models.Step;
import com.example.svarog.bakingapp.network.RecipeListClient;
import com.example.svarog.bakingapp.network.RecipeRequest;

import java.util.List;

import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.example.svarog.bakingapp.helpers.DeviceOrientationUtils.isDeciceATablet;

public class MainActivity extends AppCompatActivity implements ListItemClickListener {


    @BindView(R.id.nointernet_image)
    ImageView noInternet;

    @BindDrawable(R.drawable.nointernet)
    Drawable noInternetDrawable;


    @BindView(R.id.recipes_list)
    RecyclerView recipiesList;

    @BindView(R.id.loading_indicatior)
    ProgressBar lodaingIndicator;

    FragmentManager fragmentManager;
    private RecipeListAdapter adapter;
    private List<Recipe> recipes;

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTitle(R.string.app_name);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        ButterKnife.bind(this);

        checkIfInternetIsAvailableAndDIsplayView();


    }
    //TODO : Should this method move to another class - some kind of UTILS ?
//    private boolean isDeciceATablet() {
//        boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
//        return tabletSize;
//    }

    private void checkIfInternetIsAvailableAndDIsplayView() {
        if (isNetworkAvailable(this)) {
            fragmentManager = getSupportFragmentManager();
            if (isDeciceATablet()) {
                recipiesList.setLayoutManager(new GridLayoutManager(this, 3));
            } else {
                recipiesList.setLayoutManager(new LinearLayoutManager(this));
            }
            downloadRecipies();
        } else {
            noInternet.setImageDrawable(noInternetDrawable);
            noInternet.setVisibility(View.VISIBLE);
        }
    }

    private void downloadRecipies() {

        try {

            final RecipeRequest request = RecipeListClient.getClient().create(RecipeRequest.class);

            Call<List<Recipe>> call = request.getRecipes();
            Timber.d("Before request");
            call.enqueue(new Callback<List<Recipe>>() {
                @Override
                public void onResponse(Call<List<Recipe>> call, Response<List<Recipe>> response) {
                    Timber.d("response code: " + response.code());
                    Timber.d(response.message() + " result is : " + response.raw());
                    recipes = response.body();
                    fixVideoAndImageUrlIssue();
                    lodaingIndicator.setVisibility(View.GONE);
                    adapter = new RecipeListAdapter(recipes, MainActivity.this, MainActivity.this);
                    recipiesList.setAdapter(adapter);
//                adapter.notifyDataSetChanged();
                }

                @Override
                public void onFailure(Call<List<Recipe>> call, Throwable t) {
                    Timber.e("Error during HTTP request: " + t.getMessage());
                    Toast.makeText(getApplicationContext(), R.string.http_error_message, Toast.LENGTH_SHORT).show();
                    if (t.getMessage().equals("timeout")) {
                        Timber.e("Timeout, please restart the application");
                    }
                }
            });
        } catch (Exception ex) {
            Timber.e("Error : " + ex.getMessage());
        }

    }

    private void fixVideoAndImageUrlIssue() {

        new AsyncTask<List<Recipe>,Void,List<Recipe>>(){
            @Override
            protected List<Recipe> doInBackground(List<Recipe>[] lists) {
                for (Recipe recipe : recipes) {
                    for (Step step : recipe.getSteps()) {
                        if (step.getThumbnailURL().contains(".mp4")) {
                            swapUrls(step);
                        }
                    }
                }

                return recipes;
            }
        }.execute(recipes);
    }

    private void swapUrls(Step step) {

        String c = step.getVideoURL();

        step.setVideoURL(step.getThumbnailURL());

        step.setThumbnailURL(c);
    }

    @Override
    public void onListItemClick(int clickItmeIndex) {

        Intent recipeActivity = new Intent(this, RecipeActivity.class);
        recipeActivity.putExtra("recipe", recipes.get(clickItmeIndex));
        startActivity(recipeActivity);
    }
}
