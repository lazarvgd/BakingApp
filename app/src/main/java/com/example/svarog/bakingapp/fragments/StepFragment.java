package com.example.svarog.bakingapp.fragments;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.example.svarog.bakingapp.R;
import com.example.svarog.bakingapp.helpers.DeviceOrientationUtils;
import com.example.svarog.bakingapp.models.Step;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

/**
 * Created by svarog on 20.11.17..
 */

public class StepFragment extends Fragment {
    private final int horizontalScreenOrientationToLeft = 1;
    private final int horizontalScreenOrientationToRight = 3;
    public int recipeNumber = 0;
    @BindView(R.id.button_next)
    Button nextRecipe;
    @BindView(R.id.button_previous)
    Button previousRecipe;
    @BindView(R.id.exoplayer_view)
    SimpleExoPlayerView mPlayerView;
    @BindView(R.id.step_textView)
    TextView mStepTextView;
    @BindString(R.string.next_step)
    String nextStep;
    @BindString(R.string.previous_step)
    String previousStep;

    private TrackSelector trackSelector;
    private Context context;
    private SimpleExoPlayer mExoPlayer;
    private Uri videoUri = Uri.parse("https://www.rmp-streaming.com/media/bbb-360p.mp4");
    private ArrayList<Step> recipeSteps;
    private String videoUrl;
    private String thumbnailUrl = "";
    private long exoPlayerCurrentPosition;
    private boolean shouldAutoPlay;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


//        Timber.d("Boolean that has been sent : " + getArguments().getBoolean("orientation"));

        View root = inflater.inflate(R.layout.recipe_step_fragment, container, false);
        ButterKnife.bind(this, root);

        mPlayerView.setDefaultArtwork(BitmapFactory.decodeResource(getResources(), R.drawable.missing_url));

        context = getContext();


        if (savedInstanceState != null && savedInstanceState.containsKey("recipeNumber")) {
            Timber.d("SAVED EXTRACTING");
            getRecipesAndRecipeNumberFromSavedInstanceState(savedInstanceState);

        } else {

            getRecipesAndRecipeNumberFromArgument();
        }

        if (!DeviceOrientationUtils.isDeciceATablet()) {
            changeExoPlayerViewToFullScreenIfOrientationIsHorizontalOnPhones();
        } else {
            changeExoPlayerViewToFullScreenIfOrientationIsHorizontalOnTablets();
        }


        initializeExoPlayer();

        extractMediaFromArguments();

        mStepTextView.setText(recipeSteps.get(recipeNumber).getDescription());
        Timber.d(recipeSteps.get(recipeNumber).getDescription());

        nextRecipe.setText(nextStep);
        previousRecipe.setText(previousStep);

        nextButtonOnClickListener(recipeSteps);

        previousButtonOnClickListener(recipeSteps);

        checkIfDeviceHasChangedOrientation(savedInstanceState);
        return root;

    }

    private void changeExoPlayerViewToFullScreenIfOrientationIsHorizontalOnTablets() {
        if (getDeviceOrientation() == horizontalScreenOrientationToLeft || getDeviceOrientation() == horizontalScreenOrientationToRight) {

            View decorView = getActivity().getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                    | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                    | View.SYSTEM_UI_FLAG_IMMERSIVE);


            int width = getDisplayParam(screenOrientation.HEIGHT)*4/3;
            int height = getDisplayParam(screenOrientation.HEIGHT);

            mPlayerView.setLayoutParams(new ConstraintLayout.LayoutParams(width, height));

            nextRecipe.setVisibility(View.GONE);
            previousRecipe.setVisibility(View.GONE);
            mStepTextView.setVisibility(View.GONE);
        }
    }

    private void changeExoPlayerViewToFullScreenIfOrientationIsHorizontalOnPhones() {
        Timber.d("Orientation of the screen is : " + getDeviceOrientation());
        if (getDeviceOrientation() == horizontalScreenOrientationToLeft || getDeviceOrientation() == horizontalScreenOrientationToRight) {


            View decorView = getActivity().getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                    | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                    | View.SYSTEM_UI_FLAG_IMMERSIVE);

            int width = getDisplayParam(screenOrientation.WIDHT);

            int height = getDisplayParam(screenOrientation.HEIGHT);

            mPlayerView.setLayoutParams(new ConstraintLayout.LayoutParams(width, height));

            nextRecipe.setVisibility(View.GONE);
            previousRecipe.setVisibility(View.GONE);
            mStepTextView.setVisibility(View.GONE);
        }
    }


    private int getDisplayParam(screenOrientation param) {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;

        if (param == screenOrientation.HEIGHT) {
            return height;
        }

        return width;

    }

    private void getRecipesAndRecipeNumberFromArgument() {
        recipeNumber = getArguments().getInt("page");
        recipeSteps = getArguments().getParcelableArrayList("steps");
        Timber.d("Recipe steps " + (recipeSteps != null));
    }

    private void getRecipesAndRecipeNumberFromSavedInstanceState(@Nullable Bundle savedInstanceState) {

        Timber.d("extracting from saved instance state");
        Timber.d("CHECK IF RECIPE IS NULL ? " + (recipeSteps == null) + " from ARGS " + savedInstanceState.getParcelableArrayList("recipeSteps").isEmpty());

        recipeNumber = savedInstanceState.getInt("recipeNumber");
        recipeSteps = savedInstanceState.getParcelableArrayList("recipeSteps");
        exoPlayerCurrentPosition = savedInstanceState.getLong("exoPlayerCurrentPosition", 0);
        shouldAutoPlay = savedInstanceState.getBoolean("isPlaying");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong("exoPlayerCurrentPosition", exoPlayerCurrentPosition);
        outState.putBoolean("isPlaying",shouldAutoPlay);
        outState.putInt("recipeNumber", recipeNumber);
        outState.putParcelableArrayList("recipeSteps", recipeSteps);
    }

    private void checkIfDeviceHasChangedOrientation(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            mExoPlayer.seekTo(savedInstanceState.getLong("exoPlayerCurrentPosition"));
            mExoPlayer.setPlayWhenReady(savedInstanceState.getBoolean("isPlaying"));
        }
    }

    private int getDeviceOrientation() {
        Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        return display.getRotation();

    }

    private void extractVideo() {

        Step step = recipeSteps.get(recipeNumber);
        videoUrl = step.getVideoURL();
        Timber.d("VIDEO URL : " + videoUrl);
        if (videoUrl.contains(".mp4")) {
            videoUri = Uri.parse(videoUrl);
            prepareVideo();
        } else {
            mExoPlayer.release();
        }


    }


    private void extractThumbnail() {
        Step step = recipeSteps.get(recipeNumber);
        thumbnailUrl = step.getThumbnailURL();
        if (thumbnailUrl != null && !thumbnailUrl.equals("")) {
            Bitmap loadedImg = null;

            try {
                loadedImg = Picasso.with(getContext()).load(thumbnailUrl).get();
            } catch (IOException e) {
                e.printStackTrace();
            }


            mPlayerView.setDefaultArtwork(loadedImg);

        } else {
            mPlayerView.setDefaultArtwork(BitmapFactory.decodeResource(getResources(), R.drawable.missing_url));
        }
    }

    private void previousButtonOnClickListener(final List<Step> steps) {
        previousRecipe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Timber.d("Recipe number : " + recipeNumber);
                if (recipeNumber > 1) {
                    recipeNumber--;
                    mStepTextView.setText(steps.get(recipeNumber).getDescription());
                    extractMediaFromArguments();

                }
            }

        });
    }

    private void nextButtonOnClickListener(final List<Step> steps) {
        nextRecipe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recipeNumber++;
//                Timber.d("Recipe number : " + recipeNumber);
                if (recipeNumber < steps.size()) {
                    mStepTextView.setText(steps.get(recipeNumber).getDescription());
                    Timber.d("Recipe desc : " + steps.get(recipeNumber).getDescription());
                    extractMediaFromArguments();
                } else {
                    recipeNumber = steps.size();
                }
            }
        });
    }

    private void extractMediaFromArguments() {
        extractVideo();
        extractThumbnail();
    }

    private void initializeExoPlayer() {
        if (mExoPlayer == null) {
            trackSelector = new DefaultTrackSelector();

            LoadControl loadControl = new DefaultLoadControl();

            //Missing load control attribute, need to check with my mentor
            mExoPlayer = ExoPlayerFactory.newSimpleInstance(getContext(), trackSelector);

            mPlayerView.setPlayer(mExoPlayer);


        }
    }

    private void prepareVideo() {
        MediaSource mediaSource = new ExtractorMediaSource(
                videoUri,
                new DefaultDataSourceFactory(getContext(), "007"),
                new DefaultExtractorsFactory(),
                null,
                null);

        mExoPlayer.prepare(mediaSource);
        mExoPlayer.seekTo(exoPlayerCurrentPosition);
        mExoPlayer.setPlayWhenReady(shouldAutoPlay);


    }
    private void releasePlayer() {
        shouldAutoPlay = mExoPlayer.getPlayWhenReady();
        exoPlayerCurrentPosition = mExoPlayer.getCurrentPosition();
        mExoPlayer.release();
        mExoPlayer = null;

    }


    @Override
    public void onPause() {
        super.onPause();
        releasePlayer();

    }

    @Override
    public void onResume() {
        super.onResume();
        initializeExoPlayer();
        prepareVideo();
        mExoPlayer.seekTo(exoPlayerCurrentPosition);

        mExoPlayer.setPlayWhenReady(shouldAutoPlay);


    }

    private enum screenOrientation {WIDHT, HEIGHT}
}
