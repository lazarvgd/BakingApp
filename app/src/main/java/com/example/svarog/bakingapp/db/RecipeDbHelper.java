package com.example.svarog.bakingapp.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by svarog on 9.12.17..
 */

public class RecipeDbHelper extends SQLiteOpenHelper {


    private static final String DB_NAME = "recipe.db";

    private static final int DB_VERSION = 1;


    public RecipeDbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        final String SQL_CREATE_MOVIE_TABLE_QUERY =
                "CREATE TABLE " +
                        RecipeContract.RecipeEntry.TABLE +
                        "( " +
                        RecipeContract.RecipeEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                        RecipeContract.RecipeEntry.INGREDIENT + " VARCHAR(255)," +
                        RecipeContract.RecipeEntry.QUANTITY + " REAL," +
                        RecipeContract.RecipeEntry.MEASURE + " VARCHAR(20)" + ");";

        sqLiteDatabase.execSQL(SQL_CREATE_MOVIE_TABLE_QUERY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        //This code I have found on Udacity tutorial, so I have considered that as a good practice

        if (DB_VERSION < 2) {
            sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + RecipeContract.RecipeEntry.TABLE);
            onCreate(sqLiteDatabase);
        }


    }
}
