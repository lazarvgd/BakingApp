package com.example.svarog.bakingapp.helpers;

import com.example.svarog.bakingapp.R;
import com.example.svarog.bakingapp.application.BakingApplication;

/**
 * Created by svarog on 15.12.17..
 */

public final class DeviceOrientationUtils {


    public static boolean isDeciceATablet() {
        boolean isTablet = BakingApplication.getInstance().getResources().getBoolean(R.bool.isTablet);
        return isTablet;
    }


}
