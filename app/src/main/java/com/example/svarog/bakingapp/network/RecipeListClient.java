package com.example.svarog.bakingapp.network;

import com.example.svarog.bakingapp.application.BakingApplication;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by svarog on 12.11.17..
 */

public class RecipeListClient {

    private static final String BASE_URL = BakingApplication.BASE_URL;

    private static Retrofit retrofit = null;


    public static Retrofit getClient() {
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
