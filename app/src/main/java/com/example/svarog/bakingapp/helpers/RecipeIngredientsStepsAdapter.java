package com.example.svarog.bakingapp.helpers;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.svarog.bakingapp.R;
import com.example.svarog.bakingapp.models.Ingredient;
import com.example.svarog.bakingapp.models.Step;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by svarog on 19.11.17..
 */

public class RecipeIngredientsStepsAdapter extends RecyclerView.Adapter<RecipeIngredientsStepsAdapter.IngredientStepViewHolder> {

    private final ListItemClickListener listItemClickListener;
    private List<Step> steps;
    private List<Ingredient> ingredients;

    public RecipeIngredientsStepsAdapter(ListItemClickListener listItemClickListener) {
        this.listItemClickListener = listItemClickListener;
    }


    @Override
    public IngredientStepViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
       View item = LayoutInflater.from(parent.getContext())
               .inflate(R.layout.ingredient_step_viewholder,parent,false);

       return new IngredientStepViewHolder(item);
    }

    @Override
    public void onBindViewHolder(IngredientStepViewHolder holder, int position) {
        if (position == 0)
            holder.placeholder.setText(R.string.ingredients);
        else
            holder.placeholder.setText("Recipe "+(position)+" Step Description");
    }

    @Override
    public int getItemCount() {
        //1 is for ingredients in list
        return 1 + steps.size();
    }

    public List<Step> getSteps() {
        return steps;
    }

    public void setSteps(List<Step> steps) {
        this.steps = steps;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public class IngredientStepViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.ingedient_step_placeHolder)
        TextView placeholder;

        public IngredientStepViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            listItemClickListener.onListItemClick(getAdapterPosition());
        }
    }
}
