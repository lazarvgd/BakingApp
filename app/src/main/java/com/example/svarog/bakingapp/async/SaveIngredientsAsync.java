package com.example.svarog.bakingapp.async;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

import com.example.svarog.bakingapp.db.RecipeContract;
import com.example.svarog.bakingapp.models.Ingredient;

import java.util.List;

import timber.log.Timber;

/**
 * Created by svarog on 12.12.17..
 */

public class SaveIngredientsAsync extends AsyncTask<Void, Void, Boolean> {


    private final List<Ingredient> ingredients;
    private final Context context;

    public SaveIngredientsAsync(Context context, List<Ingredient> ingredientsList) {
        this.ingredients = ingredientsList;
        this.context = context;
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
//                        super.onPostExecute(aBoolean);
        if (!aBoolean) {
            Timber.d("Persisting data...");
            persistIngredients();
        }
        else{

            deletePrevoisRecordsFromDb();
            persistIngredients();
        }

    }

    private void deletePrevoisRecordsFromDb() {
        context.getContentResolver().delete(RecipeContract.RecipeEntry.CONTENT_URI, null, null);
    }

    private void persistIngredients() {
        for (Ingredient ingredient : ingredients) {
            ContentValues values = new ContentValues();
            values.put(RecipeContract.RecipeEntry.INGREDIENT, ingredient.getIngredient());
            values.put(RecipeContract.RecipeEntry.QUANTITY,ingredient.getQuantity());
            values.put(RecipeContract.RecipeEntry.MEASURE,ingredient.getMeasure());
            Uri uri = context.getContentResolver().insert(RecipeContract.RecipeEntry.CONTENT_URI, values);
            Timber.d("Data persisted with uri : " + uri);
        }
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        String[] projection = {RecipeContract.RecipeEntry.INGREDIENT};
        boolean hasDataInDb = false;
        Cursor cursor = context.getContentResolver().query(
                RecipeContract.RecipeEntry.CONTENT_URI,
                projection,
                null,
                null,
                null);

        if (cursor != null && cursor.moveToFirst()) {
            Timber.d("cursor has next? " + cursor.moveToFirst());
            hasDataInDb = true;

        }
        return hasDataInDb;
    }
}

