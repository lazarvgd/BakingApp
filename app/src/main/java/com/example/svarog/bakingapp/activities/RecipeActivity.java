package com.example.svarog.bakingapp.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.example.svarog.bakingapp.R;
import com.example.svarog.bakingapp.fragments.RecipeFragment;
import com.example.svarog.bakingapp.helpers.DeviceOrientationUtils;
import com.example.svarog.bakingapp.helpers.RecipeListAdapter;
import com.example.svarog.bakingapp.models.Recipe;

import butterknife.ButterKnife;
import timber.log.Timber;

/**
 * Created by svarog on 19.11.17..
 */

public class RecipeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        setContentView(R.layout.activity_recipe);


//        getActionBar().setDisplayHomeAsUpEnabled(true);

        
        Recipe recipe;
        if (getIntent().hasExtra("recipe")) {
            recipe = getIntent().getParcelableExtra("recipe");
            Timber.d("Recipe found, starting fragment!");
            Bundle recipeData = new Bundle();

            if (savedInstanceState == null) {
                startRecipeFragment(recipeData, recipe);
//                if (DeviceOrientationUtils.isDeciceATablet()) {
//
//                }


            } else {
                //TODO:Can be deleted?
                Timber.d("Start fragment regularly");
            }

        } else {
            Timber.d("Missing recipe");
        }


    }

    private void startRecipeFragment(Bundle recipeData, Recipe recipe) {
        RecipeFragment recipeFragment;

        recipeData.putParcelable("recipeData", recipe);
        recipeFragment = new RecipeFragment();
        recipeFragment.setArguments(recipeData);
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.ingridients_step_list_fragment_container, recipeFragment)
                .commit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (DeviceOrientationUtils.isDeciceATablet())
            this.finish();
    }
}
