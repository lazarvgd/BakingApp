package com.example.svarog.bakingapp.helpers;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.svarog.bakingapp.R;
import com.example.svarog.bakingapp.models.Ingredient;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by svarog on 21.11.17..
 */

public class IngredientListAdapter extends RecyclerView.Adapter<IngredientListAdapter.IngredientViewHolder> {

    private List<Ingredient> ingredients;

    public IngredientListAdapter(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    @Override
    public IngredientViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View item = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.ingredient_viewholder, parent, false);

        return new IngredientViewHolder(item);
    }

    @Override
    public void onBindViewHolder(IngredientViewHolder holder, int position) {

        holder.ingredient.setText(ingredients.get(position).getIngredient());

        holder.measure.setText(ingredients.get(position).getMeasure());

        holder.quantity.setText(""+ingredients.get(position).getQuantity());

    }

    @Override
    public int getItemCount() {
        return ingredients.size();
    }

    public class  IngredientViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.ingredient_textview)
        TextView ingredient;
        @BindView(R.id.measure_textview)
        TextView measure;
        @BindView(R.id.quantity_textview)
        TextView quantity;

        public IngredientViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


    }
}
