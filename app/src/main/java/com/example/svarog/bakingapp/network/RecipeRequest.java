package com.example.svarog.bakingapp.network;

import com.example.svarog.bakingapp.models.Recipe;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by svarog on 11.11.17..
 */

public interface RecipeRequest {

    @GET("topher/2017/May/59121517_baking/baking.json")
    Call<List<Recipe>> getRecipes();
}
