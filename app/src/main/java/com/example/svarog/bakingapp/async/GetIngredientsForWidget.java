package com.example.svarog.bakingapp.async;

import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;

import com.example.svarog.bakingapp.db.RecipeContract;

import timber.log.Timber;

/**
 * Created by svarog on 15.12.17..
 */

public class GetIngredientsForWidget extends AsyncTask<Void, Void, StringBuilder> {


    private final Context context;

    public GetIngredientsForWidget(Context context) {
        this.context = context;
    }

    @Override
    protected StringBuilder doInBackground(Void... voids) {
        StringBuilder ingredients = new StringBuilder("");
        String[] projection = {RecipeContract.RecipeEntry.INGREDIENT,RecipeContract.RecipeEntry.MEASURE,RecipeContract.RecipeEntry.QUANTITY};
        Cursor cursor =
                context.
                        getContentResolver().
                        query(
                                RecipeContract.RecipeEntry.CONTENT_URI,
                                projection,
                                null,
                                null,
                                null);

        while (cursor.moveToNext()) {
            ingredients.append(cursor.getString(cursor.getColumnIndex("ingredient")) + " - " + cursor.getString(cursor.getColumnIndex("quantity")) + " - " + cursor.getString(cursor.getColumnIndex("measure")) + ";\n");
        }
        Timber.d("Ingredients are : " + ingredients.toString());
        return ingredients;
    }



}
