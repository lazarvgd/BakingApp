package com.example.svarog.bakingapp;

import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.example.svarog.bakingapp.activities.MainActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by svarog on 15.12.17..
 */

@RunWith(AndroidJUnit4.class)
public class RecipeListBasicTest {

    @Rule
    public ActivityTestRule<MainActivity> mainActivityActivityTestRule = new ActivityTestRule<MainActivity>(MainActivity.class);


    @Test
    public void checkIfConstraintLayoutIsDisplayed_returnsTrue() {
        //find view
        onView(withId(R.id.constaint_layout_main_activity)).check(matches(isDisplayed()));
        //perform action

    }


    @Test
    public void checkIfProgressBarIsVisible_returnsGone() {
        //find view
        //perform action
        onView(withId(R.id.loading_indicatior)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE)));

    }


}
